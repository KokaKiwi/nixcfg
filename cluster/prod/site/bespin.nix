{ config, pkgs, ... }:

{
  deuxfleurs.siteName = "bespin";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.5.254";
  deuxfleurs.cnameTarget = "bespin.site.deuxfleurs.fr.";

  # Allow all ports from gateway for janky UPnP/IGD daemon
  # networking.firewall = {
  #   extraCommands = ''
  #     iptables -A INPUT -s ${cfg.staticIPv4.defaultGateway} -p udp -j ACCEPT
  #   '';
  #   extraStopCommands = ''
  #     iptables -D INPUT -s ${cfg.staticIPv4.defaultGateway} -p udp -j ACCEPT
  #   '';
  # };
}
