{ config, pkgs, ... }:

{
  deuxfleurs.siteName = "scorpio";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.1.254";
  deuxfleurs.cnameTarget = "scorpio.site.deuxfleurs.fr.";
  deuxfleurs.publicIPv4 = "82.65.41.110";
}
