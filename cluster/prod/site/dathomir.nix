{ ... }:
{
  deuxfleurs.siteName = "dathomir";
  deuxfleurs.cnameTarget = "dathomir.site.deuxfleurs.fr";
  deuxfleurs.publicIPv4 = "82.64.238.84";
  deuxfleurs.staticIPv4.defaultGateway = "192.168.1.1";
}
