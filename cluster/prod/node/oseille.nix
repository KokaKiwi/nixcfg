{ ... }:
{
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  services.openssh.ports = [ 22 33602 ];

  deuxfleurs.hostName = "oseille";
  deuxfleurs.staticIPv4.address = "192.168.1.35";
  deuxfleurs.staticIPv6.address = "2a01:e0a:5e4:1d0:223:24ff:feaf:f90b";
}
