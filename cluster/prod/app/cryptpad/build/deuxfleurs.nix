{ name ? "deuxfleurs/cryptpad"
, tag ? "nix-latest"
}: let
  sources = import ./npins;
  pkgs = import sources.nixpkgs {};
in rec {
  cryptpad = pkgs.callPackage ./default.nix {};
  docker = import ./docker.nix {
    inherit pkgs;
    inherit name tag;
    inherit cryptpad;
    withOnlyOffice = true;
  };
}
