job "cryptpad" {
  datacenters = ["neptune"]
  type = "service"

  group "cryptpad" {
    count = 1

    network {
      port "http" {
        to = 3000
      }
    }

    restart {
      attempts = 10
      delay    = "30s"
    }

    task "main" {
      driver = "docker"

      constraint {
        attribute = "${attr.unique.hostname}"
        operator = "="
        value = "courgette"
      }

      config {
        image = "kokakiwi/cryptpad:2024.6.1"
        ports = [ "http" ]

        volumes = [
          "/mnt/ssd/cryptpad:/mnt",
          "secrets/config.js:/cryptpad/config.js",
        ]
      }
      env {
        CRYPTPAD_CONFIG = "/cryptpad/config.js"
      }

      template {
        data = file("../config/config.js")
        destination = "secrets/config.js"
      }

      /* Disabled because it requires modifications to the docker image and I do not want to invest the time yet
      template {
        data = file("../config/application_config.js")
        destination = "secrets/config.js"
      }
      */

      resources {
        memory = 1000
        cpu = 500
      }

      service {
        name = "cryptpad"
        port = "http"
        tags = [
          "tricot pad.deuxfleurs.fr",
          "tricot pad-sandbox.deuxfleurs.fr",
          "tricot-add-header Cross-Origin-Resource-Policy cross-origin",
          "tricot-add-header Cross-Origin-Embedder-Policy require-corp",
          "d53-cname pad.deuxfleurs.fr",
          "d53-cname pad-sandbox.deuxfleurs.fr",
        ]
        check {
          type = "http"
          path = "/"
          interval = "10s"
          timeout = "2s"
        }
      }
    }
  }
}
