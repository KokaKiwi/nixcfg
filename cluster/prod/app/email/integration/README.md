# Email

## TLS TLS Proxy

Required for Android 7.0 that does not support elliptic curves.

Generate a key:

```bash
openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes -keyout rsa.key -out rsa.crt -subj "/CN=imap.deuxfleurs.fr" -addext "subjectAltName=DNS:smtp.deuxfleurs.fr"
```

Run the command:

```bash
./integration/proxy.sh imap.deuxfleurs.fr:993 1993
```

Test it:

```bash
openssl s_client localhost:1993
```
