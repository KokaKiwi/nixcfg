#!/bin/bash

if [[ ! -f /etc/ssl/certs/postfix.crt || ! -f /etc/ssl/private/postfix.key ]]; then
  cd /root
  openssl req \
    -new \
    -newkey rsa:4096 \
    -days 3650 \
    -nodes \
    -x509 \
    -subj ${TLSINFO} \
    -keyout postfix.key \
    -out postfix.crt
  
  mkdir -p /etc/ssl/{certs,private}/

  cp postfix.crt /etc/ssl/certs/postfix.crt 
  cp postfix.key /etc/ssl/private/postfix.key 
  chmod 400 /etc/ssl/certs/postfix.crt 
  chmod 400 /etc/ssl/private/postfix.key
fi

# A way to map files inside the postfix folder :s
for file in $(ls /etc/postfix-conf); do
  cp /etc/postfix-conf/${file} /etc/postfix/${file}
done

echo ${MAILNAME} > /etc/mailname
postmap /etc/postfix/transport

exec "$@"
