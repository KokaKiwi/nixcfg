```
sudo docker build -t superboum/amd64_opendkim:v1 .
```

```
sudo docker run -t -i \
  -v `pwd`/conf:/etc/dkim \
  -v /dev/log:/dev/log \
  -p 8999:8999
  superboum/amd64_opendkim:v1
  opendkim -f -v -x /etc/opendkim.conf
```
