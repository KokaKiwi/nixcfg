FROM golang:1.19.3-buster as builder

ARG VERSION

ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
WORKDIR /tmp/alps

RUN git init && \
    git remote add origin https://git.deuxfleurs.fr/Deuxfleurs/alps.git && \
    git fetch --depth 1 origin ${VERSION} && \
    git checkout FETCH_HEAD

RUN go build -a -o /usr/local/bin/alps ./cmd/alps

FROM scratch
COPY --from=builder /usr/local/bin/alps /alps
COPY --from=builder /tmp/alps/themes /themes
COPY --from=builder /tmp/alps/plugins /plugins
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ENTRYPOINT ["/alps"]
