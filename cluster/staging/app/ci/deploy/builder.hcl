job "builder" {
  namespace = "ci"
  type = "batch"

  datacenters = ["neptune", "dathomir", "corrin", "bespin"]
  priority = 100

  parameterized {
    payload = "optional"
    meta_required = [ "REPO_URL", "COMMIT", "BRANCH", "FLAVOR" ]
  }

  reschedule {
    attempts = 0
    unlimited = false
  }

  task "runner" {
    driver = "docker"
    config {
      image = "nixpkgs/nix:nixos-22.11"
      args = [ "/tmp/builder.sh" ]
      volumes = [ 
        "local/builder.sh:/tmp/builder.sh",
        "local/nix.conf:/etc/nix/nix.conf",
        "local/secrets:/var/run/secrets/albatros",
      ]
    }

    dispatch_payload {
      file = "secrets/secret.txt"
    }

    template {
      data = <<EOH
#!/usr/bin/env bash
set -euxo pipefail

mkdir /workspace
cd /workspace
git init 
git remote add origin ${NOMAD_META_REPO_URL}
git fetch origin ${NOMAD_META_COMMIT}
git checkout ${NOMAD_META_COMMIT} -b ${NOMAD_META_BRANCH}
export COMMIT=${NOMAD_META_COMMIT}
export BRANCH=${NOMAD_META_BRANCH}
export REPO_URL=${NOMAD_META_REPO_URL}
export FLAVOR=${NOMAD_META_FLAVOR}
if [[ -s /var/run/secrets/albatros/secret.txt ]]; then
  export SECRET_PATH=/var/run/secrets/albatros/secret.txt
fi
./.albatros

      EOH
      destination = "local/builder.sh"
      perms = "555"
    }

    template {
      data = <<EOH
substituters = https://cache.nixos.org https://nix.web.deuxfleurs.fr
trusted-public-keys = cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= nix.web.deuxfleurs.fr:eTGL6kvaQn6cDR/F9lDYUIP9nCVR/kkshYfLDJf1yKs=
max-jobs = auto
cores = 0
log-lines = 200
filter-syscalls = false
sandbox = false
keep-outputs = true
keep-derivations = true
experimental-features = nix-command flakes
      EOH
      destination = "local/nix.conf"
    }

    restart {
      attempts = 0
    }

    resources {
        cpu    = 1000
        memory = 4000
    }
  }
}

