job "plume-blog" {
  datacenters = ["neptune"]
  type = "service"

  constraint {
    attribute = "${attr.cpu.arch}"
    value     = "amd64"
  }

  group "plume" {
    count = 1

    network {
      port "web_port" { }
    }

    task "restore-db" {
      lifecycle {
        hook = "prestart"
        sidecar = false
      }

      driver = "docker"
      config {
        image = "litestream/litestream:0.3.7"
        args = [
          "restore", "-config", "/etc/litestream.yml", "/ephemeral/plume.db"
        ]
        volumes = [
          "../alloc/data:/ephemeral",
          "secrets/litestream.yml:/etc/litestream.yml"
        ]
      }
	  user = "0"

      template {
        data = file("../config/litestream.yml")
        destination = "secrets/litestream.yml"
      }

      resources {
        memory = 100
        memory_max = 1000
        cpu = 1000
      }
    }

    task "plume" {
      driver = "docker"
      config {
        image = "lxpz/devplume:v5"
        network_mode = "host"
        ports = [ "web_port" ]
        command = "sh"
        args = [ "-c", "plm search init; plm search refill; plume" ]
        volumes = [
		  "../alloc/data:/ephemeral"
        ]
      }
	  user = "0"

      template {
        data = file("../config/app.env")
        destination = "secrets/app.env"
        env = true
      }

      resources {
        memory = 200
        memory_max = 800
        cpu = 100
      }

      service {
        name = "plume"
        tags = [
          "plume",
          "tricot plume.staging.deuxfleurs.org",
          "d53-cname plume.staging.deuxfleurs.org",
        ]
        port = "web_port"
        address_mode = "host"
        check {
          type = "http"
          protocol = "http"
          port = "web_port"
          path = "/"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "600s"
            ignore_warnings = false
          }
        }
      }
      restart {
        interval = "30m"
        attempts = 20
        delay    = "15s"
        mode     = "delay"
      }
    }

    task "replicate-db" {
      driver = "docker"
      config {
        image = "litestream/litestream:0.3.7"
        args = [
          "replicate", "-config", "/etc/litestream.yml"
        ]
        volumes = [
          "../alloc/data:/ephemeral",
          "secrets/litestream.yml:/etc/litestream.yml"
        ]
      }
	  user = "0"

      template {
        data = file("../config/litestream.yml")
        destination = "secrets/litestream.yml"
      }

      resources {
        memory = 200
        memory_max = 1000
        cpu = 100
      }
    }
  }
}

