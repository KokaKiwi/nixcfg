{ ... }:
{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "origan";
  deuxfleurs.staticIPv4.address = "192.168.1.33";
  deuxfleurs.staticIPv6.address = "2a01:e0a:5e4:1d0:223:24ff:feaf:fdec";
  deuxfleurs.isRaftServer = true;

  system.stateVersion = "22.11";
}
