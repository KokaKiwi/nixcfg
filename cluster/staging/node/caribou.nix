# Configuration file local to this node

{ config, pkgs, ... }:

{
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.timeout = 20;
  boot.loader.efi.canTouchEfiVariables = true;

  deuxfleurs.hostName = "caribou";
  deuxfleurs.staticIPv6.address = "2a01:e0a:2c:540::23";
  deuxfleurs.isRaftServer = true;

  system.stateVersion = "21.05";
}
