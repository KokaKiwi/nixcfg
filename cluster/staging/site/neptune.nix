{ config, pkgs, ... }:

{
  deuxfleurs.siteName = "neptune";
  deuxfleurs.cnameTarget = "neptune.site.staging.deuxfleurs.org.";
}
