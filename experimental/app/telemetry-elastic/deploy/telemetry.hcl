job "telemetry" {
  datacenters = ["neptune"]
  type = "service"

  group "kibana" {
    count = 1

    network {
      port "kibana" {
        static = 5601
      }
    }

    task "kibana" {
      driver = "docker"
      config {
        image = "docker.elastic.co/kibana/kibana:8.2.0"
        network_mode = "host"
        ports = [ "kibana" ]
      }

      template {
        data = <<EOH
SERVER_NAME={{ env "attr.unique.hostname" }}
ELASTICSEARCH_HOSTS=http://localhost:9200
ELASTICSEARCH_USERNAME=kibana_system
ELASTICSEARCH_PASSWORD={{ key "secrets/telemetry/elastic_passwords/kibana_system" }}
SERVER_PUBLICBASEURL=https://kibana.home.adnab.me
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 1000
        cpu = 500
      }

      service {
        tags = [
            "kibana",
            "tricot kibana.staging.deuxfleurs.org",
        ]
        port = 5601
        address_mode = "driver"
        name = "kibana"
        check {
          type = "tcp"
          port = 5601
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }
  }
  
  group "grafana" {
    count = 1

    network {
      port "grafana" {
        static = 3333
      }
    }

    task "restore-db" {
      lifecycle {
        hook = "prestart"
        sidecar = false
      }

      driver = "docker"
      config {
        image = "litestream/litestream:0.3.7"
        args = [
          "restore", "-config", "/etc/litestream.yml", "/ephemeral/grafana.db"
        ]
        volumes = [
          "../alloc/data:/ephemeral",
          "secrets/litestream.yml:/etc/litestream.yml"
        ]
      }
      user = "472"

      template {
        data = file("../config/grafana-litestream.yml")
        destination = "secrets/litestream.yml"
      }

      resources {
        memory = 200
        cpu = 1000
      }
    }

    task "grafana" {
      driver = "docker"
      config {
        image = "grafana/grafana:8.4.3"
        network_mode = "host"
        ports = [ "grafana" ]
        volumes = [
          "../alloc/data:/var/lib/grafana",
          "secrets/elastic.yaml:/etc/grafana/provisioning/datasources/elastic.yaml"
        ]
      }

      template {
        data = file("../config/grafana/provisioning/datasources/elastic.yaml")
        destination = "secrets/elastic.yaml"
      }

      template {
        data = <<EOH
GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-simple-json-datasource,grafana-piechart-panel,grafana-worldmap-panel,grafana-polystat-panel
GF_SERVER_HTTP_PORT=3333
EOH
        destination = "secrets/env"
        env = true
      }

      resources {
        memory = 500
        cpu = 100
      }

      service {
        tags = [
            "grafana",
            "tricot grafana.staging.deuxfleurs.org",
        ]
        port = 3333
        address_mode = "driver"
        name = "grafana"
        check {
          type = "tcp"
          port = 3333
          address_mode = "driver"
          interval = "60s"
          timeout = "5s"
          check_restart {
            limit = 3
            grace = "90s"
            ignore_warnings = false
          }
        }
      }
    }

    task "replicate-db" {
      driver = "docker"
      config {
        image = "litestream/litestream:0.3.7"
        args = [
          "replicate", "-config", "/etc/litestream.yml"
        ]
        volumes = [
          "../alloc/data:/ephemeral",
          "secrets/litestream.yml:/etc/litestream.yml"
        ]
      }
      user = "472"

      template {
        data = file("../config/grafana-litestream.yml")
        destination = "secrets/litestream.yml"
      }

      resources {
        memory = 200
        cpu = 100
      }
    }
  }
}
